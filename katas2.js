function add(a, b) {
  return a + b;
}

console.log("adding 2+4 = " + add(2, 4));

function multiply(a, b) {
  let result = 0;
  for (let counter = 1; counter <= b; counter++) {
    result = add(result, a);
  }

  return result;
}

// a = multiply(6, 8);

console.log("multiplying 6*8 = " + multiply(6, 8));

function power(x, n) {
  let result = 1;
  for (let counter = 1; counter <= n; counter++) {
    result = multiply(result, x);
  }
  return result;
}
console.log("3^4 = " + power(3, 4));
// Comment about
function factorial(x) {
  let result = 1;
  for (let counter = x; counter >= 1; counter--) {
    result = multiply(result, counter);
  }
  
  return result;

}
console.log("factorial " + factorial(8));

function fibonacci (n) {
  let result = 1;
  for (let counter = 1; counter <= n; counter++) {
    result = add(n-1 + n-2);
  }
  
  return result;

}
console.log("fibonacci " + fibonacci(8));
 